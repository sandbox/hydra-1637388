Views responsive data tables
http://drupal.org/sandbox/Hydra/1637388
====================================


DESCRIPTION
-----------


REQUIREMENTS
------------
Drupal 7.x
views - http://drupal.org/project/views

INSTALLING
----------
1. To install the module copy the 'jcarousel_responsive_skin' folder to your sites/all/modules directory.

2. Read more about installing modules at http://drupal.org/node/70151


REPORTING ISSUE. REQUESTING SUPPORT. REQUESTING NEW FEATURE
-----------------------------------------------------------
1. Go to the module issue queue at http://drupal.org/project/issues/1637388?status=All&categories=All
2. Click on CREATE A NEW ISSUE link.
3. Fill the form.
4. To get a status report on your request go to http://drupal.org/project/issues/user


UPGRADING
---------
Read more at http://drupal.org/node/250790
