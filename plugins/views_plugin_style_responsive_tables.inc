<?php

/**
 * @file
 * Extends the table with the responsive style.
 */

/**
 * Style plugin to render each item as a row in a table.
 *
 * @ingroup views_style_plugins
 */
class views_plugin_style_responsive_tables extends views_plugin_style_table {

  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
  }

  function query() {
    parent::query();
  }

  /**
   * Generate the CSS Code.
   */
  function pre_render($result) {
    parent::pre_render($result);
    $css = '';
    $count = 1;

    foreach ($this->display->handler->get_field_labels() as $field => $label) {
      $css .= '.views-table td:nth-of-type(' . $count . '):before { content: "' . $label . '"; }';
      $count++;
    }

    drupal_add_css(drupal_get_path('module', 'views_responsive_data_tables') . '/css/views-responsive-data-tables.css');
    drupal_add_css('
      @media only screen and (max-width: 760px), (min-device-width: 768px) and (max-device-width: 1024px)  {
        /* Label the data */
        ' . $css . '
      }
    ', array(
      'group' => CSS_THEME,
      'type' => 'inline',
      'media' => 'screen',
      'preprocess' => FALSE,
    ));
  }

}

