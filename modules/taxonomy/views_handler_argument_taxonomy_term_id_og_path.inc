<?php

/**
 * @file
 * Contains the 'taxonomy term' argument validator plugin.
 */

/**
 * Validate whether an argument is an acceptable node.
 */
class views_handler_argument_taxonomy_term_id_og_path extends views_plugin_argument_validate_taxonomy_term {

  function init(&$view, &$argument, $options) {
    parent::init($view, $argument, $options);
  }

  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  function options_form(&$form, &$form_state) {
    $form['type']['#options']['path'] = ('Term path converted to Term ID');
  }

  function options_submit(&$form, &$form_state, &$options = array()) {

  }

  function convert_options(&$options) {

  }

  function validate_argument($argument) {
    $type = $this->options['type'];

    if ($type == 'path') {
      $path = explode('/', drupal_get_normal_path(arg(0) . '/' . arg(1) . '/' . arg(2)));
      $tid = $path[2];
      $this->argument->argument = $tid;
      return TRUE;
    }
    return FALSE;
  }

  function process_summary_arguments(&$args) {

  }

}
