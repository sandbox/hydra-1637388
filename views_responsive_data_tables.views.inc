<?php
/**
 * @file
 * Register our plugins with Views.
 */

/**
 * Implementation of hook_views_plugins().
 */
function views_responsive_data_tables_views_plugins() {
  return array(
    'module' => 'views_responsive_data_tables',
    'style' => array(
      'responsive_tables' => array(
        'path' => drupal_get_path('module', 'views_responsive_data_tables') . '/plugins',
        'parent' => 'table',
        'title' => t('Responsive data tables'),
        // Use original
        'theme' => 'views_view_table',
        'help' => t('Foobar'),
        'handler' => 'views_plugin_style_responsive_tables',
        'uses row plugin' => TRUE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
      ),
    ),
  );
}
