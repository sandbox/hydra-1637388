<?php
/**
 * @file
 * Register our plugins with Views.
 */


/**
 * Implementation of hook_views_data_alter().
 */
function argument_taxonomy_term_id_og_views_data_alter(&$data) {
  $data['taxonomy_term_hierarchy']['parent2'] = array(
    'title' => t('Parent term 2'),
    'help' => t('The parent term of the term. This can produce duplicate entries if you are using a vocabulary that allows multiple parents.'),
    'argument' => array(
      'help' => t('The parent term of the term with path.'),
      'handler' => 'views_handler_argument_taxonomy_term_id_og_path',
    ),
  );
}

/**
 * Implementation of hook_views_handlers() to register all of the basic handlers
 * views uses.
 */
function argument_taxonomy_term_id_og_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'argument_taxonomy_term_id_og') . '/modules/taxonomy',
    ),
    'handlers' => array(
      'views_handler_argument_taxonomy_term_id_og_path' => array(
        'parent' => 'views_plugin_argument_validate_taxonomy_term',
      ),
    ),
  );
}
